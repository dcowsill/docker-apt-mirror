# docker-apt-mirror

docker-apt-mirror is an image and Compose stack to initialize, serve and maintain an Ubuntu or Debian mirror using apt-mirror.

Read more about it [here](https://dcowsill.com/post/building-an-ubuntu-mirror-with-docker/).